	/* Creation du socket ipv4 type TCP protocole TPC*/
	/*retourne le descripteur de la socket*/
	
#include "socket.h"
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/wait.h>




int socket(int domain, int type, int protocol);
//Attacher la socket à une interface reseau
int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
//Ecoute - Atente de connexion
int listen(int sockfd, int backlog);
//Sockets clients
int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
//Option socket
int setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len);


int creer_serveur(int port){
    printf("Serveur en Service\n");
	//printf("Serveur\n" );
	

	int socket_serveur ;
	socket_serveur = socket ( AF_INET , SOCK_STREAM , 0);
	if ( socket_serveur == -1){ // Si la création de la socket a échoué
		/* traitement de l ’ erreur */
		perror ( " socket_serveur " );
		return -1;
	}
/* Attachement de la socket serveur sur toutes les interfaces*/	
	struct sockaddr_in saddr ;
	saddr.sin_family = AF_INET ; /* Socket ipv4 */
	saddr.sin_port = htons (port); /* Port d ’écoute */
	saddr.sin_addr.s_addr = INADDR_ANY ; /* é coute sur toutes les interfaces */

	int optval = 1;
	if ( setsockopt ( socket_serveur , SOL_SOCKET , SO_REUSEADDR , & optval , sizeof ( int )) == -1){
		perror ( " Can not set SO_REUSEADDR option " );	
		return -1;
	}

	if ( bind ( socket_serveur , ( struct sockaddr *)& saddr , sizeof ( saddr )) == -1){
		perror ( " bind socker_serveur " );
		/* traitement de l ’ erreur */
		return -1;
	}
	/* Attente de connexions*/
    /* Lancer l'attente de connexion*/
	if ( listen ( socket_serveur , 10) == -1){	//accepte jusque 10 clients
		perror ( " listen socket_serveur " );
		
		/* traitement d ’ erreur */
		return -1;
	}
	
	return socket_serveur;
}

